package be.kdg.pro2;

public class Dish {
	private String name="anonymous";
	private int persons;
	private Spice spice;

	public Dish(String name, int persons, Spice spice) {
		setName(name);
		this.persons = persons;
		this.spice = spice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			throw new NullPointerException();
		} else if (name.length()<3){
			throw new IllegalArgumentException();
		}
		this.name=name;
	}

	public int getPersons() {
		return persons;
	}

	public void setPersons(int persons) {
		this.persons = persons;
	}

	public Spice getSpice() {
		return spice;
	}

	public void setSpice(Spice spice) {
		this.spice = spice;
	}

	@Override
	public String toString() {
		return "Dish{" +
			"name='" + name.toUpperCase() + '\'' +
			", persons=" + persons +
			", spice='" + spice + '\'' +
			'}';
	}
}
