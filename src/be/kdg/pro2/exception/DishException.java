package be.kdg.pro2.exception;

public class DishException extends Exception{
	public DishException() {
	}

	public DishException(String message, Throwable cause) {
		super(message, cause);
	}
}
