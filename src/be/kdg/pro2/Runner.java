package be.kdg.pro2;

import be.kdg.pro2.exception.DishException;

import java.util.ArrayList;
import java.util.List;

import static be.kdg.pro2.Spice.*;

public class Runner {
	public static void main(String[] args) throws DishException{

		Dish waterzooi = new Dish("Waterzooi", 4, PEPPER);
		Dish mattentaart = new Dish("Mattentaart", 2, CINNAMON);
		Dish speculoos = new Dish("Speculoos", 2, CINNAMON);
		Dish spaghetti = new Dish("Spaghetti", 2, PEPPER);
		List<Dish> dishes = new ArrayList<>(List.of(waterzooi,mattentaart,speculoos,spaghetti));
		dishes.add(new Dish("Chili con carne", 8, CHILI));
		System.out.println(dishes);
		for(Spice spice : Spice.values()){
			System.out.printf("%s (%d), ",spice,spice.ordinal());
		}
		System.out.println("\nPeppered dishes");
		for(Dish dish:dishes){
			if(dish.getSpice()== PEPPER){
				System.out.println("\t" + dish);
			}
		}

		System.out.println("\nHot dishes");
		for(Dish dish:dishes){
			if(dish.getSpice().hotness >=5){
				System.out.println("\t" + dish);
			}
		}
		try {
			dishes.add(new Dish(null, 0, CHILI));
		} catch (NullPointerException e) {
			throw new DishException("Name cannot be null",e);
		}catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
			System.err.println("Name must be at least 3 letters");;
		}
		try {
			System.out.println(dishes);
		} catch (NullPointerException e) {
			System.out.println("Exception printing dishes" + e);;
		}

	}
}
