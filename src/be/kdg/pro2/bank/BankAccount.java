package be.kdg.pro2.bank;

public class BankAccount {
	private double balance;
	private Safe safe;

	public BankAccount(double balance, Safe safe) {
		this.balance = balance;
		this.safe = safe;
	}


	public Safe getSafe() {
		return safe;
	}

	public void setSafe(Safe safe) {
		this.safe = safe;
	}

	@Override
	public String toString() {
		return "BankAccount{" +
			"safe=" + safe +
			'}';
	}
}
