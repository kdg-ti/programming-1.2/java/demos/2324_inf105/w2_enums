package be.kdg.pro2;

public class DishWithConstants {
	private String name;
	private int persons;
	private int spice;

	public DishWithConstants(String name, int persons, int spice) {
		this.name = name;
		this.persons = persons;
		this.spice = spice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPersons() {
		return persons;
	}

	public void setPersons(int persons) {
		this.persons = persons;
	}

	public int getSpice() {
		return spice;
	}

	public void setSpice(int spice) {
		this.spice = spice;
	}

	@Override
	public String toString() {
		return "DishWithConstants{" +
			"name='" + name + '\'' +
			", persons=" + persons +
			", spice='" + spice + '\'' +
			'}';
	}
}
