package be.kdg.pro2;

public enum Spice {
	PEPPER("poivre",7),
	CINNAMON("cannelle",2),
	CHILI("chili",8);
	public final String name;
	public final int hotness;

	Spice(String name, int hotness) {
		this.name = name;
		this.hotness = hotness;
	}

	@Override
	public String toString() {
		return name;
	}


}
